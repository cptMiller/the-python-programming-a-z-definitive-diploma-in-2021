#!/usr/bin/env python
# coding: utf-8

# In[1]:


#first python program
print("Hello World, Hello Python")


# In[2]:


10 + 10


# # Using Jupiter Notebook

# ## Using Jupiter Notebook

# ### Using Jupiter Notebook

# In[5]:


print("Hello Jupiter notebook")


# # Adding Two Number

# In[7]:


num1 = 3
num2 = 2.5

# add two numbers 
sum = num1 + num2

# print the number 
print("the sum of {0} and {1} is {2}" .format(num1, num2, sum))


# # Python Indentifier

# In[10]:


number = 5


# In[11]:


number


# ### The variable must contain lower case alphabetical first and then the number.... don't use the special operator for naming your variables

# # Python Statements

# In[13]:


x = 1 + 2 + 3 + 4 +     5 + 6 + 7 +     8 + 9


# In[14]:


x


# In[15]:


y = (5 + 10 +
     15 + 20 + 
    25 + 30)


# In[16]:


y


# In[19]:


fruits = ["Apple", "Cherry", "Guava", "Mango", "Kiwi"]


# In[20]:


fruits


# # Indentation

# In[23]:


for i in range(0, 10): 
    print(i)
    if i == 6:
        break


# # Comments

# In[24]:


#this is comment 
print("hi")


# In[25]:


#this is comment 
#it can't be process within the code script 


# In[ ]:




